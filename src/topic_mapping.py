# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC UPPER ORIGINAL.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

# created variables for storing the outputs and al ist of all outcomes.
topic_map = {}
all_outcomes = []

# Collects a list of all outcomes.
for row in range(2, ws_input.max_row):
    # Iterates over the outcomes columns.
    for column in range(99, 119):
        outcome = ws_input.cell(row=row, column=column).value

        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = outcome[0]
            if outcome.upper() not in all_outcomes:
                all_outcomes.append(outcome.upper())
        except IndexError:
            break

# Sorts list of outcomes in alphabetical order.
all_outcomes.sort()

# Adds headings to the output workbook.
for col in range(0, len(all_outcomes)):
    ws_output.cell(row=1, column=col + 2).value = all_outcomes[col]

# Populates the dictionary with incidences of each outcome.
for row in range(2, ws_input.max_row):
    # Iterates over the outcomes columns.
    temp = []
    for column in range(99, 119):
        outcome = ws_input.cell(row=row, column=column).value
        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = outcome[0]
            temp.append(outcome.upper())
        except IndexError:
            break
    # Creates an undirected graph of data for outcome frequencies.
    for outcome in temp:
        if outcome not in topic_map:
            topic_map[outcome] = {}
            for item in all_outcomes:
                topic_map[outcome][item] = 0
        for other_outcome in temp:
            if outcome != other_outcome:
                topic_map[outcome][other_outcome] = topic_map[outcome][other_outcome] + 1

# Writes data to the output workbook.
row = 2
for key_one in sorted(topic_map):
    col = 2
    ws_output.cell(row=row, column=1).value = key_one
    for key_two in topic_map[key_one]:
        ws_output.cell(row=row, column=col).value = topic_map[key_one][key_two]
        col = col + 1
    row = row + 1

# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/topic_map.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
