# Imports for regular expressions.
import re

# Input string made shorter for the sake of readability.
input_string = "ab	ad	am	an	as	ca	cfi	csi	cim	dp	dmi	did	ncd	gbvp	gbvv	gl	glf	hia	hiv	ha	oc	Pi	rsa	rfi	sh	sei	sti	sts	aod	sis	tp	up	vc	o"
result = ""

# removal of whitespace and splitting into tokens.
re.sub(r'\s+', ' ', input_string).strip()
outcomes = input_string.split()

# Reformats into proper python formatting.
for item in outcomes:
    result = result + "\"" + item + "\": 0, "
print(result)
