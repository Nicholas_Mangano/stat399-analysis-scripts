# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/ref_matrix.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

comp_array = {1: {1: 0, 0: 0}, 0: {1: 0, 0: 1}}

# print(ws_input.cell(row=1, column=1).value)
# print(ws_input.cell(row=1, column=33).value)
# print(ws_input.cell(row=1, column=34).value)
# print(ws_input.cell(row=1, column=66).value)

for row in range(2, ws_input.max_row):
    for col in range(1, 33):
        ref = ws_input.cell(row=row, column=col).value
        pref = ws_input.cell(row=row, column=col + 33).value
        comp_array[ref][pref] = comp_array[ref][pref] + 1

for key1 in comp_array:
    for key2 in comp_array[key1]:
        print(comp_array[key1][key2])

