# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/ref_matrix.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

list_output = []

for row in range(2, ws_input.max_row):
    temp = {1: {1: 0, 0: 0}, 0: {1: 0, 0: 1}}
    for col in range(1, 33):
        ref = ws_input.cell(row=row, column=col).value
        pref = ws_input.cell(row=row, column=col + 33).value
        temp[ref][pref] = temp[ref][pref] + 1
    list_output.append(temp)

row = 1
for case in list_output:
    print(case)
    ws_output.cell(row=row, column=1).value = case[1][1]
    ws_output.cell(row=row, column=2).value = case[1][1] + case[1][0] + case[0][1]
    row = row + 1

# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/case_agreement.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
