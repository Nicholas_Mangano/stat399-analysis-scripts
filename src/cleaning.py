#These are the packages that you would need to first install and import into python
import pandas as pd
from pandas import DataFrame
import numpy as np
import datetime

#Importing excel files or .xlsx use this
#make sure to specify the location of your file
df = pd.read_excel("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC.xlsx")

#Upper Case entire dataframe
df = df.apply(lambda x: x.str.upper() if x.dtype == "object" else x)

#Export dataframe as an excel file
df.to_excel("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC_Upper.xlsx", sheet_name='%d r' % (len(df)), index=False)
