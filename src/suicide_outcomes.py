# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC UPPER ORIGINAL.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

# List of categories as per the provided documentation.
mandatory_outcomes = ["S1", "S8", "S10", "S11"]
suicide_outcomes = ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13"]

ws_output.cell(row=1, column=1).value = "total"
ws_output.cell(row=1, column=2).value = "important"
for col in range(3, len(suicide_outcomes)):
    ws_output.cell(row=1, column=col).value = suicide_outcomes[col]

# List for storing python dictionaries
list_of_cases = []
total_suicide_cases = 0
output = []

# iterates over the data in the input workbook by row.
for row in range(2, ws_input.max_row):
    # Creates a temporary dictionary for the storing of frequency data.
    temp = []
    # Iterates over the objectives columns.
    for column in range(99, 119):
        outcome = ws_input.cell(row=row, column=column).value
        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = outcome[0]
            if outcome.upper() in suicide_outcomes:
                temp.append(outcome.upper())
        except IndexError:
            break
    if len(temp) > 0:
        list_of_cases.append(temp)

for case in list_of_cases:
    temp = {"total": 0, "important": 0, "S1":0, "S2":0, "S3":0, "S4":0, "S5":0,
            "S6":0, "S7":0, "S8":0, "S9":0, "S10":0, "S11":0, "S12":0, "S13":0}
    temp["total"] = len(case)
    important = 0
    for outcome in case:
        if outcome in suicide_outcomes:
            temp[outcome] = 1
            if outcome in mandatory_outcomes:
                important = important + 1
    temp["important"] = important
    output.append(temp)

row = 2
for case in output:
    col = 1
    for key in case:
        ws_output.cell(row=row, column=col).value = case[key]
        col = col + 1
    row = row + 1

# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/suicide_data.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
