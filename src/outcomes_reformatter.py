# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC UPPER ORIGINAL.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

# List of categories as per the provided documentation.
categories = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'M', 'P', 'R', 'S', 'T']

# List for storing python dictionaries
list_of_dicts = []

# Adds headings to the output workbook.
for col in range(0, len(categories)):
    ws_output.cell(row=1, column=col + 1).value = categories[col]

# iterates over the data in the input workbook by row.
for row in range(2, ws_input.max_row):

    # Creates a temporary dictionary for the storing of frequency data.
    temp_dict = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0,
                 'I': 0, 'J': 0, 'K': 0, 'M': 0, 'P': 0, 'R': 0, 'S': 0, 'T': 0}

    # Iterates over the objectives columns.
    for column in range(99, 119):
        value = ws_input.cell(row=row, column=column).value

        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = value[0]
            if identifier in categories:
                temp_dict[identifier] = temp_dict[identifier] + 1
        except IndexError:
            break

    # Writes data to the output workbook.
    for col in range(0, len(categories)):
        ws_output.cell(row=row, column=col + 1).value = temp_dict[categories[col]]
    list_of_dicts.append(temp_dict)

# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC UPPER ORIGINAL FM.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
