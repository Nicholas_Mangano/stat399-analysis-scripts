# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC CURRENT.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

# List of categories as per the provided documentation.
all_outcomes = ["A1", "A10", "A11", "A12", "A13", "A14", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "B1", "B10",
                "B11", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "C1", "C10", "C11", "C12", "C13", "C14", "C15",
                "C16", "C17", "C18", "C19", "C2", "C20", "C21", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "D1", "D10",
                "D11", "D12", "D13", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "E1", "E2", "E3", "E4", "F1", "F2",
                "F3", "F4", "F5", "F6", "F7", "F8", "F9", "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "H1", "H10",
                "H11", "H12", "H13", "H14", "H15", "H16", "H17", "H18", "H19", "H2", "H3", "H4", "H5", "H6", "H7", "H8",
                "H9", "I1", "I2", "I3", "I4", "I5", "I6", "I7", "I8", "J1", "J10", "J11", "J2", "J3", "J4", "J5", "J6",
                "J7", "J8", "J9", "K1", "K10", "K11", "K12", "K13", "K14", "K15", "K16", "K17", "K18", "K19", "K2",
                "K3", "K4", "K5", "K6", "K7", "K8", "K9", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "P1", "P12", "P13",
                "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "R1", "R10", "R2", "R3", "R4", "R5", "R6", "R7", "R8",
                "R9", "S1", "S10", "S11", "S12", "S13", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "T1", "T2",
                "T3", "T4", "T5", "T6", "T7"]

# List for storing python dictionaries
list_of_dicts = []
dict_of_outcomes = {"A1": 0, "A10": 0, "A11": 0, "A12": 0, "A13": 0, "A14": 0, "A2": 0, "A3": 0, "A4": 0, "A5": 0, "A6": 0,
            "A7": 0, "A8": 0, "A9": 0, "B1": 0, "B10": 0, "B11": 0, "B2": 0, "B3": 0, "B4": 0, "B5": 0, "B6": 0,
            "B7": 0, "B8": 0, "B9": 0, "C1": 0, "C10": 0, "C11": 0, "C12": 0, "C13": 0, "C14": 0, "C15": 0, "C16": 0,
            "C17": 0, "C18": 0, "C19": 0, "C2": 0, "C20": 0, "C21": 0, "C3": 0, "C4": 0, "C5": 0, "C6": 0, "C7": 0,
            "C8": 0, "C9": 0, "D1": 0, "D10": 0, "D11": 0, "D12": 0, "D13": 0, "D2": 0, "D3": 0, "D4": 0, "D5": 0,
            "D6": 0, "D7": 0, "D8": 0, "D9": 0, "E1": 0, "E2": 0, "E3": 0, "E4": 0, "F1": 0, "F2": 0, "F3": 0, "F4": 0,
            "F5": 0, "F6": 0, "F7": 0, "F8": 0, "F9": 0, "G1": 0, "G2": 0, "G3": 0, "G4": 0, "G5": 0, "G6": 0, "G7": 0,
            "G8": 0, "H1": 0, "H10": 0, "H11": 0, "H12": 0, "H13": 0, "H14": 0, "H15": 0, "H16": 0, "H17": 0, "H18": 0,
            "H19": 0, "H2": 0, "H3": 0, "H4": 0, "H5": 0, "H6": 0, "H7": 0, "H8": 0, "H9": 0, "I1": 0, "I2": 0, "I3": 0,
            "I4": 0, "I5": 0, "I6": 0, "I7": 0, "I8": 0, "J1": 0, "J10": 0, "J11": 0, "J2": 0, "J3": 0, "J4": 0,
            "J5": 0, "J6": 0, "J7": 0, "J8": 0, "J9": 0, "K1": 0, "K10": 0, "K11": 0, "K12": 0, "K13": 0, "K14": 0,
            "K15": 0, "K16": 0, "K17": 0, "K18": 0, "K19": 0, "K2": 0, "K3": 0, "K4": 0, "K5": 0, "K6": 0, "K7": 0,
            "K8": 0, "K9": 0, "M1": 0, "M2": 0, "M3": 0, "M4": 0, "M5": 0, "M6": 0, "M7": 0, "P1": 0, "P12": 0,
            "P13": 0, "P2": 0, "P3": 0, "P4": 0, "P5": 0, "P6": 0, "P7": 0, "P8": 0, "P9": 0, "R1": 0, "R10": 0,
            "R2": 0, "R3": 0, "R4": 0, "R5": 0, "R6": 0, "R7": 0, "R8": 0, "R9": 0, "S1": 0, "S10": 0, "S11": 0,
            "S12": 0, "S13": 0, "S2": 0, "S3": 0, "S4": 0, "S5": 0, "S6": 0, "S7": 0, "S8": 0, "S9": 0, "T1": 0,
            "T2": 0, "T3": 0, "T4": 0, "T5": 0, "T6": 0, "T7": 0}

# Adds headings to the output workbook.
for col in range(0, len(all_outcomes)):
    ws_output.cell(row=1, column=col + 1).value = all_outcomes[col]

# iterates over the data in the input workbook by row.
for row in range(2, ws_input.max_row):
    # Iterates over the objectives columns.
    for column in range(107, 127):
        value = ws_input.cell(row=row, column=column).value
        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = value[0]
            if value.upper() in dict_of_outcomes:
                dict_of_outcomes[value.upper()] = dict_of_outcomes[value.upper()] + 1
        except IndexError:
            break

# Writes data to the output workbook.
col = 1
for key in dict_of_outcomes:
    ws_output.cell(row=2, column=col).value = dict_of_outcomes[key]
    col = col + 1

# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/outcome_frequency_simple.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
