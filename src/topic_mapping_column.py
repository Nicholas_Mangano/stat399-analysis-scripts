# Imports the ability to parse excel files.
import openpyxl
from openpyxl import Workbook

# Opens the input file for parsing.
wb_input = openpyxl.load_workbook("C:/Users/Nicholas/PycharmProjects/STAT399/data/GC UPPER ORIGINAL.xlsx")
ws_input = wb_input.active

# Creates an output workbook for storing data.
wb_output = Workbook()
ws_output = wb_output.active

# created variables for storing the outputs and al ist of all outcomes.
topic_map = {}
all_outcomes = ["A1", "A10", "A11", "A12", "A13", "A14", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "B1", "B10",
                "B11", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "C1", "C10", "C11", "C12", "C13", "C14", "C15",
                "C16", "C17", "C18", "C19", "C2", "C20", "C21", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "D1", "D10",
                "D11", "D12", "D13", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "E1", "E2", "E3", "E4", "F1", "F2",
                "F3", "F4", "F5", "F6", "F7", "F8", "F9", "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "H1", "H10",
                "H11", "H12", "H13", "H14", "H15", "H16", "H17", "H18", "H19", "H2", "H3", "H4", "H5", "H6", "H7", "H8",
                "H9", "I1", "I2", "I3", "I4", "I5", "I6", "I7", "I8", "J1", "J10", "J11", "J2", "J3", "J4", "J5", "J6",
                "J7", "J8", "J9", "K1", "K10", "K11", "K12", "K13", "K14", "K15", "K16", "K17", "K18", "K19", "K2",
                "K3", "K4", "K5", "K6", "K7", "K8", "K9", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "P1", "P12", "P13",
                "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "R1", "R10", "R2", "R3", "R4", "R5", "R6", "R7", "R8",
                "R9", "S1", "S10", "S11", "S12", "S13", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "T1", "T2",
                "T3", "T4", "T5", "T6", "T7"]

# Populates the dictionary with incidences of each outcome.
for row in range(2, ws_input.max_row):
    # Iterates over the outcomes columns.
    temp = []
    for column in range(99, 119):
        outcome = ws_input.cell(row=row, column=column).value
        # Try except clause for breaking the loop when you encounter a blank record.
        try:
            identifier = outcome[0]
            if outcome in all_outcomes:
                temp.append(outcome.upper())
        except IndexError:
            break
    # Creates an undirected graph of data for outcome frequencies.
    for outcome in temp:
        if outcome not in topic_map:
            topic_map[outcome] = {}
            for item in all_outcomes:
                topic_map[outcome][item] = 0
        for other_outcome in temp:
            if outcome != other_outcome:
                topic_map[outcome][other_outcome] = topic_map[outcome][other_outcome] + 1

# Writes data to the output workbook.

row = 1
for key_one in sorted(topic_map):
    for key_two in topic_map[key_one]:
        ws_output.cell(row=row, column=1).value = key_one
        ws_output.cell(row=row, column=2).value = key_two
        ws_output.cell(row=row, column=3).value = topic_map[key_one][key_two]
        row = row + 1
# Saves the output workbook.
wb_output.save("C:/Users/Nicholas/PycharmProjects/STAT399/data/topic_map_column.xlsx")

# Closes the open workbooks.
wb_input.close()
wb_output.close()
